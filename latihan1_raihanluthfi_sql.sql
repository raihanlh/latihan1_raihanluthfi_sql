--
-- PostgreSQL database dump
--

-- Dumped from database version 13.4 (Ubuntu 13.4-1.pgdg20.04+1)
-- Dumped by pg_dump version 13.4 (Ubuntu 13.4-1.pgdg20.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: kelasbe; Type: TABLE; Schema: public; Owner: raihanlh
--

CREATE TABLE public.kelasbe (
    id integer NOT NULL,
    nama character varying,
    tema character varying,
    alamat character varying
);


ALTER TABLE public.kelasbe OWNER TO raihanlh;

--
-- Name: kelasbe_id_seq; Type: SEQUENCE; Schema: public; Owner: raihanlh
--

CREATE SEQUENCE public.kelasbe_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.kelasbe_id_seq OWNER TO raihanlh;

--
-- Name: kelasbe_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: raihanlh
--

ALTER SEQUENCE public.kelasbe_id_seq OWNED BY public.kelasbe.id;


--
-- Name: kelasbe id; Type: DEFAULT; Schema: public; Owner: raihanlh
--

ALTER TABLE ONLY public.kelasbe ALTER COLUMN id SET DEFAULT nextval('public.kelasbe_id_seq'::regclass);


--
-- Data for Name: kelasbe; Type: TABLE DATA; Schema: public; Owner: raihanlh
--

COPY public.kelasbe (id, nama, tema, alamat) FROM stdin;
1	Budi	OOP	Jakarta
2	Andi	Database	Surabaya
4	Dika	OOP	Bandung
5	Asep	Database	Bandung
3	Nama telah diubah	Spring Boot	Jakarta
\.


--
-- Name: kelasbe_id_seq; Type: SEQUENCE SET; Schema: public; Owner: raihanlh
--

SELECT pg_catalog.setval('public.kelasbe_id_seq', 5, true);


--
-- PostgreSQL database dump complete
--

